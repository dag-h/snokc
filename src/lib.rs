use serde::{Deserialize, Serialize};
use std::process;
use std::str::FromStr;
use strum::EnumString;

#[derive(Clone, PartialEq, Deserialize, Serialize, Debug)]
pub enum AddrMode {
    Immediate = 0,
    Direct = 1,
    Indirect = 2,
    Register = 3,
    Implied = 4,
    None,
}

/// Represents an actual token present in an expression. Some tokens
/// contain their respective value.
#[derive(Clone, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub enum Token {
    Op(Operator),
    Reg(Register),
    Num(u16),
    Addr(u16),
    IndirectAddr(u16),
    Const(String),
    ConstDef(String, Box<Token>),
    Label(String),
    LabelDef(String),
    Comma,
    None,
}

/// Represents a token present in a valid grammar definition. The value of a
/// token does not influence its validitiy, which is why this enum does not contain
/// the token value. This is what differentiates TokenType from Token.
#[derive(Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub enum TokenType {
    Op,
    Reg,
    Num,
    Addr,
    IndirectAddr,
    Const,
    ConstDef,
    Label,
    Comma,
}

/// Valid registers with their respective bit values, used for register based addressing.
/// Total number of registers must not be changed.
#[derive(Clone, Debug, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub enum Register {
    R1 = 0,
    R2 = 1,
    R3 = 2,
    R4 = 3,
    R5 = 4,
    R6 = 5,
    R7 = 6,
    R8 = 7,
    Any,
}

impl From<u8> for Register {
    fn from(n: u8) -> Self {
        match n {
            1 => Register::R1,
            2 => Register::R2,
            3 => Register::R3,
            4 => Register::R4,
            5 => Register::R5,
            6 => Register::R6,
            7 => Register::R7,
            8 => Register::R8,
            _ => {
                eprintln!("Register index out of range, must be in range 1 to 8");
                process::exit(1);
            }
        }
    }
}

/// Valid operations with their respective bit value. Total number of operators must
/// never exceed 2^5 = 32.
#[derive(Clone, Debug, Eq, Hash, PartialEq, EnumString, Deserialize, Serialize)]
#[strum(serialize_all = "lowercase")]
pub enum Operator {
    Nop = 0,
    Jmp = 1,
    Call = 2,
    Beq = 3,
    Bne = 4,
    Bpl = 5,
    Bmi = 6,
    Add = 7,
    And = 8,
    Sub = 9,
    Cmp = 10,
    Inc = 11,
    Dec = 12,
    Mul = 13,
    Mov = 14,
    Or = 15,
    Ret = 16,
    Reti = 17,
    Ld = 18,
    St = 19,
    Push = 20,
    Pop = 21,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct OpGrammar {
    pub expr: Vec<TokenType>,
    pub mode: AddrMode,
}

#[derive(Debug, PartialEq)]
struct Label(String);

#[derive(Debug, PartialEq)]
pub struct Parsed {
    pub expr: Vec<Token>,
    pub mode: AddrMode,
}

pub fn is_const_def(s: &str) -> Result<Option<(String, Token)>, SnokError> {
    let mut toks = s.split_whitespace();
    match toks.next() {
        Some(tok) => {
            if tok == "#def" {
                let ident;
                let value;
                match toks.next() {
                    Some(id) => {
                        if id.to_uppercase() == id {
                            ident = id;
                        } else {
                            return Err(SnokError::ConstDefError(
                                "Identifier for const definition must be uppercase".to_string(),
                            ));
                        }
                    }
                    None => {
                        return Err(SnokError::ConstDefError(
                            "Did not supply identifier for const definition".to_string(),
                        ));
                    }
                }
                match toks.next() {
                    Some(val) => match get_token(val)? {
                        Token::Reg(reg) => value = Token::Reg(reg),
                        Token::Num(num) => value = Token::Num(num),
                        Token::Addr(addr) => value = Token::Addr(addr),
                        Token::IndirectAddr(addr) => value = Token::IndirectAddr(addr),
                        _ => {
                            return Err(SnokError::ConstDefError(
                                "Invalid token type for const value".to_string(),
                            ));
                        }
                    },
                    None => {
                        return Err(SnokError::ConstDefError(
                            "Did not supply value for const definition".to_string(),
                        ));
                    }
                }
                Ok(Some((ident.into(), value)))
            } else {
                Ok(None)
            }
        }
        None => Ok(None),
    }
}

pub fn is_label_def(s: &str) -> Option<String> {
    if s.len() >= 2 {
        let s = s.trim();
        let mut chs = s.chars();
        if chs.any(|c| c.is_ascii_whitespace()) {
            return None;
        }
        let mut chs = s.chars();
        if chs.all(|c| c.is_ascii()) {
            let mut chs = s.chars();
            if chs.next().unwrap().is_alphabetic() && chs.last().unwrap() == ':' {
                Some(s.trim_end_matches(':').into())
            } else {
                None
            }
        } else {
            None
        }
    } else {
        None
    }
}

fn is_label(s: &str) -> Option<String> {
    let s = s.trim();
    let mut chs = s.chars();
    if chs.any(|c| c.is_ascii_whitespace()) {
        return None;
    }
    if s.to_lowercase() == s && s.is_ascii() {
        Some(s.into())
    } else {
        None
    }
}

fn is_reg(s: &str) -> Option<u8> {
    if s.len() == 2 {
        let s = s.trim().to_lowercase();
        let mut s = s.chars();
        // unwrap okay since we know the iterator will have two elements
        let r = s.next().unwrap();
        let num: u8 = match s.next().unwrap().to_digit(10) {
            Some(num) => num as u8,
            None => return None,
        };
        if r == 'r' && num <= 8 && num >= 1 {
            Some(num)
        } else {
            None
        }
    } else {
        None
    }
}

fn is_addr(s: &str) -> Option<u16> {
    let s = s.trim().replace("_", "");
    let mut toks = s.chars();
    match toks.next() {
        None => None,
        Some(c) => {
            if c == '$' {
                if toks.all(|c| c.is_digit(10)) && s.len() <= 5 {
                    Some(u16::from_str_radix(s.trim_start_matches('$'), 10).unwrap())
                } else {
                    None
                }
            } else {
                None
            }
        }
    }
}

fn is_indirect_addr(s: &str) -> Option<u16> {
    let s = s.trim().replace("_", "");
    let mut toks = s.chars();
    match toks.next() {
        None => None,
        Some(c) => {
            if c == '@' {
                if toks.all(|c| c.is_digit(10)) && s.len() <= 5 {
                    Some(u16::from_str_radix(s.trim_start_matches('@'), 10).unwrap())
                } else {
                    None
                }
            } else {
                None
            }
        }
    }
}

fn is_num(s: &str) -> Option<u16> {
    if s.trim().chars().all(|c| c.is_digit(10)) {
        match s.parse() {
            Ok(num) => Some(num),
            Err(_) => None,
        }
    } else if s.starts_with("0b") {
        let num = s.trim_start_matches("0b");
        if num.chars().all(|c| c.is_digit(2)) {
            match u16::from_str_radix(num, 2) {
                Ok(n) => Some(n),
                Err(_) => None,
            }
        } else {
            None
        }
    } else if s.starts_with("0x") {
        let num = s.trim_start_matches("0x");
        if num.chars().all(|c| c.is_digit(16)) {
            match u16::from_str_radix(num, 16) {
                Ok(n) => Some(n),
                Err(_) => None,
            }
        } else {
            None
        }
    } else {
        None
    }
}

fn is_const(s: &str) -> Option<String> {
    let s = s.trim();
    let mut chs = s.chars();
    if chs.all(|c| c.is_ascii()) {
        if s.to_uppercase() == s {
            Some(s.to_string())
        } else {
            None
        }
    } else {
        None
    }
}

pub fn get_token(s: &str) -> Result<Token, SnokError> {
    if let Some(val) = is_num(s) {
        Ok(Token::Num(val))
    } else if let Ok(val) = Operator::from_str(s) {
        Ok(Token::Op(val))
    } else if let Some(val) = is_reg(s) {
        Ok(Token::Reg(Register::from(val)))
    } else if let Some(val) = is_addr(s) {
        Ok(Token::Addr(val))
    } else if let Some(val) = is_indirect_addr(s) {
        Ok(Token::IndirectAddr(val))
    } else if let Some(val) = is_const(s) {
        Ok(Token::Const(val))
    } else if let Some(val) = is_label(s) {
        Ok(Token::Label(val))
    } else if s.trim() == "," {
        Ok(Token::Comma)
    } else {
        Err(SnokError::InvalidToken(s.into()))
    }
}

pub fn to_string(p: &Parsed) -> Result<String, SnokError> {
    let toks = &p.expr;
    let op = match toks.get(0) {
        Some(Token::Op(op)) => op.clone() as u8,
        // first token in expression should always be an operator if it is to be
        // compiled to machine code
        _ => {
            return Err(SnokError::CompilationError(
                "First token should be an operator".to_string(),
            ));
        }
    };

    let mode = match &p.mode {
        AddrMode::Implied => 0,
        m => m.clone() as u8,
    };

    let reg;
    let addr;
    match toks.get(1) {
        Some(Token::Reg(r)) => {
            reg = r.clone() as u8;
            addr = match toks.get(3) {
                None => 0,
                Some(Token::Num(n)) => *n,
                Some(Token::Addr(a)) => *a,
                Some(Token::IndirectAddr(ia)) => *ia,
                Some(Token::Reg(r)) => r.clone() as u16,
                Some(other) => {
                    return Err(SnokError::CompilationError(format!(
                        "Could not parse to string: Invalid second operand: {:?}",
                        other
                    )));
                }
            }
        }
        None => {
            reg = 0;
            addr = 0;
        }
        Some(Token::Num(n)) => {
            reg = 0;
            addr = *n;
        }
        Some(Token::Addr(a)) => {
            reg = 0;
            addr = *a;
        }
        Some(Token::IndirectAddr(ia)) => {
            reg = 0;
            addr = *ia;
        }
        Some(other) => {
            return Err(SnokError::CompilationError(format!(
                "An error occurred when parsing to string: Invalid first operand: {:?}",
                other
            )));
        }
    };

    let output = format!("{:05b}{:02b}{:03b}{:016b}", op, mode, reg, addr);

    Ok(output)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_string_works() {
        let parsed = Parsed {
            expr: vec![
                Token::Op(Operator::Add),
                Token::Reg(Register::R2),
                Token::Comma,
                Token::Reg(Register::R3),
            ],
            mode: AddrMode::Register,
        };
        assert_eq!(
            to_string(&parsed),
            Ok("00111110010000000000000010".to_string())
        );
    }

    #[test]
    fn strum_works() {
        assert_eq!(Operator::Nop, Operator::from_str("nop").unwrap());
        assert_eq!(Operator::Call, Operator::from_str("call").unwrap());
        assert_eq!(Operator::Reti, Operator::from_str("reti").unwrap());
    }

    #[test]
    fn is_reg_works() {
        assert_eq!(Some(1), is_reg("r1"));
        assert_eq!(Some(2), is_reg("r2"));
        assert_eq!(Some(3), is_reg("r3"));
        assert_eq!(Some(4), is_reg("r4"));
        assert_eq!(Some(5), is_reg("r5"));
        assert_eq!(Some(6), is_reg("r6"));
        assert_eq!(Some(7), is_reg("r7"));
        assert_eq!(Some(8), is_reg("r8"));
        assert!(is_reg("asdf").is_none());
        assert!(is_reg("r0").is_none());
        assert!(is_reg("r9").is_none());
        assert!(is_reg("r12").is_none());
    }

    #[test]
    fn is_addr_works() {
        assert_eq!(Some(0x1020), is_addr("$1020"));
        assert_eq!(Some(0xAF10), is_addr("$AF10"));
        assert_eq!(Some(0xB109), is_addr("$B109"));
        assert_eq!(Some(0x2), is_addr("$2"));
        assert!(is_addr("A").is_none());
        assert!(is_addr("$QWER").is_none());
        assert!(is_addr("123").is_none());
        assert!(is_addr("asdgf100101001").is_none());
        assert!(is_addr("$01100011001010011").is_none());
        assert!(is_addr("$011000110010100111010101001001001").is_none());
    }

    #[test]
    fn is_const_def_works() {
        assert_eq!(
            Ok(Some((format!("TEST"), Token::Num(100)))),
            is_const_def("#def TEST 100")
        );
        assert_eq!(
            Ok(Some((format!("MYCONST"), Token::Num(1234)))),
            is_const_def("#def MYCONST 1234")
        );
        assert_eq!(
            Ok(Some((format!("MYCONST"), Token::Reg(Register::R1)))),
            is_const_def("#def MYCONST r1")
        );
        assert_eq!(
            Ok(Some((format!("MYCONST"), Token::Addr(0x1030)))),
            is_const_def("#def MYCONST $1030")
        );
        assert_eq!(
            Ok(Some((format!("MYCONST"), Token::IndirectAddr(0xAF10)))),
            is_const_def("#def MYCONST @AF10")
        );
        assert_eq!(Ok(None), is_const_def("adsf"));
        assert_eq!(Ok(None), is_const_def("hello 1234"));
    }

    #[test]
    fn is_label_works() {
        assert_eq!(Some(format!("loop")), is_label("loop"));
        assert_eq!(Some(format!("function3")), is_label("function3"));
        assert_eq!(Some(format!("test123test")), is_label("test123test"));
        assert_eq!(None, is_label("funct ion"));
    }
}

#[derive(Debug, PartialEq)]
pub enum SnokError {
    InvalidToken(String),
    ConstDefError(String),
    ValidationError { line: u16, msg: String },
    CompilationError(String),
    ReadError(String),
}

impl std::fmt::Display for SnokError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::InvalidToken(tok) => write!(f, "Invalid token: {}", tok),
            Self::ConstDefError(msg) => write!(f, "{}", msg),
            Self::ValidationError { line: _, msg } => write!(f, "{}", msg),
            Self::CompilationError(msg) => write!(f, "{}", msg),
            Self::ReadError(msg) => write!(f, "{}", msg),
        }
    }
}
