use snokc::{
    get_token, is_const_def, is_label_def, to_string, AddrMode, OpGrammar, Operator, Parsed,
    SnokError, Token, TokenType,
};
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead, Write};
use std::process;

fn main() {
    let mut args = std::env::args();
    args.next();

    let file_name = match args.next() {
        Some(f) => f,
        None => {
            eprintln!("Please provide an input file");
            process::exit(1);
        }
    };

    let input = match File::open(file_name) {
        Ok(file) => io::BufReader::new(file).lines(),
        Err(err) => {
            eprintln!("An error occurred when opening file: {}", err);
            process::exit(1);
        }
    };

    let mut exprs: Vec<Vec<Token>> = Vec::new();
    for (i, line) in input.enumerate() {
        let line_nr = i + 1;
        let line = match line {
            Ok(l) => l,
            Err(err) => {
                eprintln!(
                    "Line {}: An error occurred when reading line: {}",
                    line_nr, err
                );
                process::exit(1);
            }
        };
        let line = line.trim();

        // disregard empty lines
        if line.is_empty() {
            continue;
        }

        // disregard comments
        if line.starts_with(';') {
            continue;
        }

        let mut toks: Vec<Token> = Vec::new();

        // correct const definitions and label definitions are on a single line,
        // so these are checked before iterating over tokens
        match is_const_def(line) {
            Ok(Some((ident, val))) => {
                toks.push(Token::ConstDef(ident, Box::new(val)));
                exprs.push(toks);
                continue;
            }
            // this occcurs if it was found to be a const definition, but some of its formatting
            // is incorrect
            Err(err) => {
                eprintln!(
                    "Line {}: An error occurred when parsing const definition: {}",
                    line_nr, err
                );
                process::exit(1);
            }
            Ok(None) => (),
        }

        if let Some(name) = is_label_def(line) {
            toks.push(Token::LabelDef(name));
            exprs.push(toks);
            continue;
        }

        for tok in line.split_whitespace() {
            // should have a comma after first operand if two operands
            if tok.ends_with(',') {
                let mut tok = tok.to_string();
                tok.pop();
                // finding correct token
                match get_token(&tok) {
                    Ok(token) => toks.push(token),
                    Err(err) => {
                        eprintln!(
                            "Line {}: An error occurred when parsing token: {}",
                            line_nr, err
                        );
                        process::exit(1);
                    }
                }
                toks.push(Token::Comma);
            } else {
                match get_token(tok) {
                    Ok(token) => toks.push(token),
                    Err(err) => {
                        eprintln!(
                            "Line {}: An error occurred when parsing token: {}",
                            line_nr, err
                        );
                        process::exit(1);
                    }
                }
            }
        }
        exprs.push(toks);
    }

    // maps const definition identifiers to their value (a token)
    let mut const_defs: HashMap<String, Token> = HashMap::new();
    // maps label names to the line number it is present on
    let mut labels: HashMap<String, u16> = HashMap::new();

    let file = match File::open("grammar.yml") {
        Ok(f) => f,
        Err(err) => {
            eprintln!("Could not open grammar file: {}", err);
            process::exit(1);
        }
    };

    // maps operators to a list of all its valid grammar formats
    let op_grammars: HashMap<Operator, Vec<OpGrammar>> =
        serde_yaml::from_reader(io::BufReader::new(file)).unwrap();

    // parsed will be a vector of Parsed objects, containing exprs with valid grammar
    let mut parsed: Vec<Parsed> =
        match validate_exprs(&exprs, &op_grammars, &mut const_defs, &mut labels) {
            Ok(parsed) => parsed,
            Err(err) => {
                if let SnokError::ValidationError { line, msg } = err {
                    eprintln!(
                        "Line {}: An error occurred during validation: {}",
                        line, msg
                    );
                    process::exit(1);
                } else {
                    // unreachable since validate_exprs should only return ValidationError variant
                    unreachable!();
                }
            }
        };
    //println!("{:#?}", labels);

    // TODO fix line numbering, will be incorrect since const/label defs are removed
    // iterates through parsed and replaces any Const or Label usages with their respective
    // actual values
    for (i, p) in parsed.iter_mut().enumerate() {
        let expr = &mut p.expr;
        let line_nr = i + 1;

        let mut new_expr = expr.clone();
        for (i, tok) in expr.iter_mut().enumerate() {
            match tok {
                Token::Const(ident) => match const_defs.get(ident) {
                    Some(t) => {
                        new_expr[i] = t.clone();
                    }
                    None => {
                        eprintln!("Line {}: Undefined constant: {}", line_nr, ident);
                        process::exit(1);
                    }
                },
                Token::Label(name) => match labels.get(name) {
                    Some(l) => {
                        new_expr[i] = Token::Addr(*l);
                    }
                    None => {
                        eprintln!("Line {}: Undefined label: {}", line_nr, name);
                        process::exit(1);
                    }
                },
                _ => (),
            }
        }

        for (i, t) in new_expr.iter().enumerate() {
            expr[i] = t.clone();
        }

        // at this point, all expressions have an operator as their first token, so this unwrap
        // is fine and else clause is unreachable
        let op = if let Token::Op(op) = expr.first().unwrap() {
            op
        } else {
            unreachable!();
        };

        // final validation of expression with all consts and labels replaced with their values
        match is_valid_expr(&expr, op_grammars.get(op).unwrap()) {
            Some(mode) => p.mode = mode,
            None => {
                eprintln!(
                    "Line {}: Expression does not match any defined grammar for operator: {:?}",
                    line_nr, op
                );
                process::exit(1);
            }
        }
    }

    // default file name in case one is not provided
    let path = match args.next() {
        Some(arg) => arg,
        None => "output".into(),
    };

    let mut file = match File::create(path) {
        Ok(file) => file,
        Err(err) => {
            eprintln!("An error occurred when opening file: {}", err);
            process::exit(1);
        }
    };

    let mut out = String::new();
    for p in parsed.iter() {
        let line = match to_string(p) {
            Ok(line) => line,
            Err(err) => {
                eprintln!("An error occurred during final compilation: {}", err);
                process::exit(1);
            }
        };
        out += &format!("{}\n", line);
    }
    file.write_all(out.as_bytes())
        .expect("Write to output file failed");
}

/// Iterates through given list of expressions and validates them according to grammar found in
/// `grammar.yml`. Also finds any const definitions and labels and adds them to the given hashmaps,
/// removing them from the final list of expressions before returning said list.
fn validate_exprs(
    exprs: &[Vec<Token>],
    op_grammars: &HashMap<Operator, Vec<OpGrammar>>,
    const_defs: &mut HashMap<String, Token>,
    labels: &mut HashMap<String, u16>,
) -> Result<Vec<Parsed>, SnokError> {
    let mut parsed_exprs = Vec::new();
    let mut line = 1;
    'expr: for expr in exprs.iter() {
        let parsed_expr: Vec<Token>;
        match expr.first() {
            Some(first) => match first {
                Token::Op(op) => {
                    match op_grammars.get(&op) {
                        Some(op_gr) => match is_valid_expr(&expr, &op_gr) {
                            Some(mode) => {
                                parsed_expr = expr.clone();
                                parsed_exprs.push(Parsed {
                                    expr: parsed_expr,
                                    mode,
                                });
                                line += 1;
                                continue 'expr;
                            }
                            None => {
                                return Err(SnokError::ValidationError { line, msg: format!("Expression does not match any defined grammar for operator: {:?}", op) });
                            }
                        },
                        None => {
                            return Err(SnokError::ValidationError {
                                line,
                                msg: format!("The instruction has no defined grammar: {:?}", op),
                            });
                        }
                    };
                }
                Token::ConstDef(ident, val) => {
                    if expr.len() == 1 {
                        const_defs.insert(ident.into(), *val.clone());
                        line -= 1;
                    } else {
                        return Err(SnokError::ValidationError {
                            line,
                            msg: "Incorrect format for const definition".to_string(),
                        });
                    }
                }
                Token::LabelDef(name) => {
                    if expr.len() == 1 {
                        line -= 1;
                        labels.insert(name.into(), line as u16);
                    } else {
                        return Err(SnokError::ValidationError {
                            line,
                            msg: "Incorrect format for label definition".to_string(),
                        });
                    }
                }
                _ => {
                    return Err(SnokError::ValidationError {
                        line,
                        msg: format!(
                            "Could not find valid format matching expression: {:?}",
                            expr
                        ),
                    });
                }
            },
            None => {
                return Err(SnokError::ValidationError {
                    line,
                    msg: "Empty expression".to_string(),
                });
            }
        };
        line += 1;
    }
    Ok(parsed_exprs)
}

/// Validates the given expression according to the given list of valid grammar.
fn is_valid_expr(expr: &[Token], grammars: &[OpGrammar]) -> Option<AddrMode> {
    'outer: for op_gr in grammars {
        let gr = &op_gr.expr;
        if expr.len() != gr.len() + 1 {
            // eprintln!(
            //     "Expression length does not match grammar length:\n expression: {:?}\ngrammar: {:?}",
            //     expr, gr
            // );
            continue 'outer;
        }
        let mut expr = expr.iter();
        expr.next();
        'inner: for (i, tok) in expr.enumerate() {
            match tok {
                Token::Op(_) => {
                    match gr.get(i) {
                        Some(TokenType::Op) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::Reg(_) => {
                    match gr.get(i) {
                        Some(TokenType::Reg) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::Num(_) => {
                    match gr.get(i) {
                        Some(TokenType::Num) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::Addr(_) => {
                    match gr.get(i) {
                        Some(TokenType::Addr) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::IndirectAddr(_) => {
                    match gr.get(i) {
                        Some(TokenType::IndirectAddr) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::Const(_) => {
                    match gr.get(i) {
                        // a Const cannot be resolved to a Comma or a ConstDef so these
                        // are the only types not allowed in place of a Const
                        Some(TokenType::Comma) => continue 'outer,
                        Some(TokenType::ConstDef) => continue 'outer,
                        _ => continue 'inner,
                    };
                }
                Token::ConstDef(_, _) => {
                    match gr.get(i) {
                        Some(TokenType::ConstDef) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                Token::Label(_) => {
                    match gr.get(i) {
                        // a Label cannot be resolved to a Comma or a ConstDef so these
                        // are the only types not allowed in place of a Label
                        Some(TokenType::Comma) => continue 'outer,
                        Some(TokenType::ConstDef) => continue 'outer,
                        _ => continue 'inner,
                    };
                }
                Token::Comma => {
                    match gr.get(i) {
                        Some(TokenType::Comma) => continue 'inner,
                        _ => continue 'outer,
                    };
                }
                _ => return None,
            }
        }
        return Some(op_gr.mode.clone());
    }
    None
}
